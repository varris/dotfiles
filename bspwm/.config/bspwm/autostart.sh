#!/usr/bin/env fish 

start-pulseaudio-x11 &
env LANG=de_AT.UTF-8 ckb-next -b &
setxkbmap de -variant nodeadkeys
xset +fp /usr/share/fonts/misc &
xset +fp ~/.fonts &
xset fp rehash &
xsetroot -cursor_name left_ptr &
xinput --set-prop 'Kingsis Peripherals ZOWIE Gaming mouse' 'libinput Accel Profile Enabled' 0, 1
autocutsel -selection CLIPBOARD -fork &
autocutsel -selection PRIMARY -fork &
~/.scripts/dunst.sh
udiskie &
mpd &
transmission-daemon &
numlockx on &
unclutter &
#steam -b &
compton --config ~/.config/compton/compton.conf &
redshift -l 48.210033:16.363449 &
$HOME/.config/polybar/launch.sh &
wal -R
~/.config/wal/scripts/ckb-updatecolors.sh &
mpd-notification -m ~/music -t 5 &
