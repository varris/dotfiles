#apply pywal colors
cat ~/.cache/wal/sequences & 

set -gx fish_user_paths /usr/lib/ccache/bin /usr/lib/llvm/8/bin ~/.scripts ~/.bin ~/.local/bin /usr/local/bin

set -gx BROWSER firefox
set -gx EDITOR nvim
set -gx OCL_ICD_VENDORS mesa
set -gx QT_QPA_PLATFORMTHEME gtk2
set -gx WINEDEBUG fixme-all
set -gx WINEDLLOVERRIDES winemenubuilder.exe=d

set -gx WINE_DIR $HOME/wine
set -gx WINE_NO_AUTOSTART 0
set -gx DXVK_BUILD_DIR $HOME/.repos/dxvk-build/dxvk-frog
set -gx DXVK_REPO_DIR $HOME/.repos/dxvk

set -gx D9VK_BUILD_DIR $HOME/.repos/d9vk-build/dxvk-frog
set -gx D9VK_REPO_DIR $HOME/.repos/d9vk

set -gx PULSE_LATENCY_MSEC 60

set -gx XDG_CONFIG_HOME /home/$USER/.config

alias n="nvim"
alias nf="neofetch"
alias odin="~/.repos/Odin/odin"
alias r="vifmrun"
alias tsr="transmission-remote"
alias um="udiskie-umount -a"
alias vifm="vifmrun"
alias watchtsr="watch -n 1 transmission-remote -l"
alias weather="curl https://wttr.in/Vienna"
alias wget="curl -O"

if status is-interactive
    switch $TERM
        # Fix DEL key in st
        case 'st*'
            set -gx is_simple_terminal 1

        case "linux"
            set -e is_simple_terminal
    end

    if set -q is_simple_terminal
        tput smkx ^/dev/null
        function fish_enable_keypad_transmit --on-event fish_postexec
            tput smkx ^/dev/null
        end

        function fish_disable_keypad_transmit --on-event fish_preexec
            tput rmkx ^/dev/null
        end
    end
end
