function cleanpkg
   if test -f "$PWD/PKGBUILD"
       echo "Cleaning package directory..."
       rm -R pkg src "*.pkg.tar.xz" ^ /dev/null
       echo "done"
   else
       echo "Not in a PKGBUILD directory."
   end
end
