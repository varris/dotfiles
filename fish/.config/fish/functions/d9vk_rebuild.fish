function d9vk_rebuild
    if ! test -d $D9VK_REPO_DIR
        echo "D9VK repo doesn't exist. Try to clone the repo and try again!"
        return 1
    end

    pushd $PWD >/dev/null
    cd "$D9VK_REPO_DIR"
    git fetch origin

    if test (git rev-parse --abbrev-ref HEAD) = "master"
        git pull origin master
    end

    cd "$HOME/.repos/d9vk-build"
    if ! test -d "$HOME/.repos/d9vk-build/d9vk-frog"
        bash "$D9VK_REPO_DIR/package-release.sh" frog "$HOME/.repos/d9vk-build" --dev-build
    end
    cd "$HOME/.repos/d9vk-build/dxvk-frog/build.32"
    ninja install
    cd "$HOME/.repos/d9vk-build/dxvk-frog/build.64"
    ninja install
    popd >/dev/null
end
