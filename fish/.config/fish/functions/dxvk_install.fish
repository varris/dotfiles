function dxvk_install
    if ! test -d "$DXVK_BUILD_DIR"
        echo "DXVK build dir is missing. Rebuild DXVK and try again!"
        return 1
    end

    yes | bash "$DXVK_BUILD_DIR/setup_dxvk.sh" install --symlink
end
