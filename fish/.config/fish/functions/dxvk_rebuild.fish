function dxvk_rebuild
    if ! test -d $DXVK_REPO_DIR
        echo "DXVK repo doesn't exist. Try to clone the repo and try again!"
        return 1
    end

    pushd $PWD >/dev/null
    cd "$DXVK_REPO_DIR"
    git fetch origin

    if test (git rev-parse --abbrev-ref HEAD) = "master"
        git pull origin master
    end

    cd "$HOME/.repos/dxvk-build"
    if ! test -d "$HOME/.repos/dxvk-build/dxvk-frog"
        bash "$DXVK_REPO_DIR/package-release.sh" frog "$HOME/.repos/dxvk-build" --dev-build
    end
    cd "$HOME/.repos/dxvk-build/dxvk-frog/build.32"
    ninja install
    cd "$HOME/.repos/dxvk-build/dxvk-frog/build.64"
    ninja install
    popd >/dev/null
end
