function ec
    fd . ~/.dotfiles -t f -H -E '.git' | fzf -q "$argv[1]" | xargs -r $EDITOR
end
