function fish_prompt
    if test (id -u) = 0
        set_color red
        printf "(ROOT) ► "
    else
        set_color cyan
        printf "► "
    end
    set_color normal
end
