function ls
    if string match '*/.dotfiles/*' $PWD >/dev/null
        exa -a $argv
    else
        exa $argv
    end
end
