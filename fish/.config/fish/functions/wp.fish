function wp
    if test (count $argv) = 0
        echo "Usage: wp wineprefix"
        return 1
    end

    set -gx WINEPREFIX "$WINE_DIR/$argv[1]"
    set -gx DXVK_HUD version,devinfo,fps
    set -gx DXVK_STATE_CACHE_PATH "$HOME/.cache/dxvk"
    set -gx PULSE_LATENCY_MSEC 60
    set -gx WINE (which wine)
    set -gx WINEDEBUG -all
    set -gx WINEESYNC 1

    set -gx __GL_SHADER_DISK_CACHE_PATH $WINEPREFIX
    set -gx MESA_GLSL_CACHE_DIR $WINEPREFIX

    if test -d "$WINE_DIR"
        if test ! -d "$WINEPREFIX/drive_c"
            read -p "echo 'Prefix $argv[1] does not exist. Create it? (y/n) '" -l ANSWER
            if string match -ri '^y(es)|^y' $ANSWER >/dev/null
                echo "Creating wine prefix $argv[1]"
                wine wineboot >/dev/null
                cd "$WINEPREFIX/drive_c/users/$USER"
                for i in "Desktop" "My Documents" "My Music" "My Pictures" "My Videos"
                    unlink "$i"
                    mkdir "$i"
                end
                cd "$WINEPREFIX/drive_c"
                echo "done"
            end
        else
            if test $WINE_NO_AUTOSTART = 0
                cd "$WINEPREFIX/drive_c"
                if test -e "$WINEPREFIX/wine_autostart.sh"
                    bash "$WINEPREFIX/wine_autostart.sh"
                end
            else
                echo "WINEPREFIX set to $WINEPREFIX"
            end
        end
    end
end

