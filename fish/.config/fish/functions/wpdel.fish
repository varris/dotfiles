function wpdel
    if test (count $argv) = 0
        echo "Usage: wpdel wineprefix"
        return 1
    end

    if test -d "$WINE_DIR/$argv[1]"
        read -p "echo 'Do you really want to remove prefix $argv[1]? (y/n) '" -l ANSWER
            if string match --regex '^[Yy]|^[YyEeSs]' $ANSWER >/dev/null
                rm -R "$WINE_DIR/$argv[1]"
                echo "done"
                cd ~
            end
    else
        echo "Prefix $argv[1] does not exist!"
    end
end
