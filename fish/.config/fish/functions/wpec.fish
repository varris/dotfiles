function wpec
    if ! count $argv >/dev/null
        echo "Usage: wpec wineprefix"
        return 1
    end

    if test ! -d "$WINE_DIR/$argv[1]"
       echo "Prefix $argv[1] does't exist!"
    else
        nvim "$WINE_DIR/$argv[1]/wine_autostart.sh"
        chmod u+x "$WINE_DIR/$argv[1]/wine_autostart.sh"
    end
end

