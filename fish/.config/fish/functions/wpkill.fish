function wpkill
    if test (count $argv) = 0
        echo "Usage: wpkill wineprefix"
        return 1
    end

    set -gx WINEPREFIX "$WINE_DIR/$argv[1]"
    wineserver -k
end
