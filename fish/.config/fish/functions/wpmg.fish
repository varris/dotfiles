function wpmg
    if ! count $argv >/dev/null
        echo "Usage: wpmg wineprefix"
        return 1
    end

    cd "$WINE_DIR/$argv[1]/drive_c/users/$USER/My Documents/My Games"
end
