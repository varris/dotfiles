"{{{ general nvim settings

let mapleader = ","
set autoindent
set clipboard=unnamedplus
set cursorline
set expandtab
set fillchars+=vert:│
set foldmethod=marker
set ignorecase
set incsearch
set mouse=""
set noswapfile
set number
set shiftwidth=4
set shortmess+=c
set tabstop=4
set title
set whichwrap+=<,>,h,l,[,]
set relativenumber
set autoread

autocmd FocusGained,BufEnter,CursorHold,CursorHoldI * if mode() != 'c' | checktime | endif
autocmd FileChangedShellPost *
  \ echohl WarningMsg | echo "File changed on disk. Buffer reloaded." | echohl None


"buftabline
set hidden

nnoremap <silent> <Space> @=(foldlevel('.')?'za':"\<Space>")<CR>
vnoremap <Space> zf
nnoremap <leader>cd :cd %:p:h<CR>:pwd<CR>

"}}} general nvim settings end

"{{{ vim-plug
call plug#begin()
Plug 'ap/vim-buftabline'
Plug 'chrisbra/csv.vim'
Plug 'dylanaraps/wal.vim'
Plug 'garbas/vim-snipmate'
Plug 'honza/vim-snippets'
Plug 'junegunn/fzf.vim'
Plug 'junegunn/vim-easy-align' 
Plug 'kovetskiy/sxhkd-vim'
Plug 'LnL7/vim-nix'
Plug 'MarcWeber/vim-addon-mw-utils'
Plug 'mklabs/split-term.vim'
Plug 'ntpeters/vim-better-whitespace'
Plug 'ryanoasis/vim-devicons'
Plug 'scrooloose/nerdcommenter'
Plug 'scrooloose/nerdtree'
Plug 'sheerun/vim-polyglot'
Plug 'tomtom/tlib_vim'
Plug 'tpope/vim-surround'
Plug 'Yggdroot/LeaderF', { 'do': './install.sh' }
Plug 'mhinz/vim-startify'
Plug 'tpope/vim-fugitive'

"NCM2
Plug 'ncm2/ncm2'
Plug 'roxma/nvim-yarp'
Plug 'ncm2/ncm2-bufword'
Plug 'ncm2/ncm2-tmux'
Plug 'ncm2/ncm2-path'
Plug 'ncm2/ncm2-racer'
Plug 'ncm2/ncm2-snipmate'
call plug#end()
"}}} vim-plug end

"{{{ plugin specific settings
let g:NERDTreeWinSize=45

nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
tnoremap <Esc> <C-\><C-n>
map <C-n> :NERDTreeToggle<CR>
xmap ga <Plug>(EasyAlign)
nmap ga <Plug>(EasyAlign)

nnoremap <F5> :buffers<CR>:buffer<Space>

"launch nerdtree if no buffer is open
"au vimenter * if !argc() | NERDTree | endif

"buftabline
let g:buftabline_show=1
let g:buftabline_numbers=2
let g:buftabline_indicators=1

nmap <leader>1 <Plug>BufTabLine.Go(1)
nmap <leader>2 <Plug>BufTabLine.Go(2)
nmap <leader>3 <Plug>BufTabLine.Go(3)
nmap <leader>4 <Plug>BufTabLine.Go(4)
nmap <leader>5 <Plug>BufTabLine.Go(5)
nmap <leader>6 <Plug>BufTabLine.Go(6)
nmap <leader>7 <Plug>BufTabLine.Go(7)
nmap <leader>8 <Plug>BufTabLine.Go(8)
nmap <leader>9 <Plug>BufTabLine.Go(9)
nmap <leader>0 <Plug>BufTabLine.Go(10)

"}}} plugin specific settings

"{{{ ncm2 settings
autocmd BufEnter * call ncm2#enable_for_buffer()
set completeopt=noinsert,menuone,noselect
noremap <c-c> <ESC>

"inoremap <expr> <CR> (pumvisible() ? "\<c-y>\<cr>" : "\<CR>")
inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"^

au User Ncm2Plugin call ncm2#register_source({
        \ 'name' : 'css',
        \ 'priority': 9, 
        \ 'subscope_enable': 1,
        \ 'scope': ['css','scss'],
        \ 'mark': 'css',
        \ 'word_pattern': '[\w\-]+',
        \ 'complete_pattern': ':\s*',
        \ 'on_complete': ['ncm2#on_complete#omni', 'csscomplete#CompleteCSS'],
        \ })

"}}} ncm2 settings end

"{{{ statusline
function! Mode()
    if &syntax == "startify" | return "startify"
    else
        let l:mode = mode()
        if     mode ==# "n"  | return "normal"
        elseif mode ==# "c"  | return "command"
        elseif mode ==# "i"  | return "insert"
        elseif mode ==# "R"  | return "replace"
        elseif mode ==# "t"  | return "terminal"
        elseif mode ==# "V"  | return "v-line"
        elseif mode ==# "v"  | return "visual"
        else                 | return l:mode
        endif
    endif
endfunc

set laststatus=2
set statusline=%1*\  
set statusline+=%{Mode()}\  
set statusline+=%*\  
set statusline+=%f 
set statusline+=%= 
set statusline+=%m
set statusline+=\ %y\ \  
set statusline+=%{&fileencoding}
set statusline+=\[%{&fileformat}\]\ \   
set statusline+=\ %p%%\ \  
set statusline+=%l\/%L\  
"}}} statusline end

"{{{ colors
colorscheme wal

highlight User1 cterm=none ctermfg=0 ctermbg=2
"highlight User2 cterm=none ctermfg=0 ctermbg=2
highlight VertSplit cterm=none ctermfg=1 ctermbg=0
highlight StatusLine cterm=none ctermfg=0 ctermbg=5
highlight CursorLine cterm=none ctermfg=none ctermbg=16
highlight CursorLineNr cterm=none ctermfg=1 ctermbg=none
highlight LineNr cterm=none ctermbg=none ctermfg=darkgrey

"buftabline
highlight BufTabLineCurrent cterm=none ctermfg=0 ctermbg=2
highlight BufTabLineFill cterm=none ctermfg=0 ctermbg=5
highlight BufTabLineHidden cterm=none ctermfg=0 ctermbg=5

"colors end }}}
