#!/usr/bin/env bash

updates=$(auracle sync | wc -l)

if [[ $updates -eq 0 ]]; then
    echo " no updates"
elif [[ $updates -eq 1 ]]; then
    echo " $updates update"
else
    echo " $updates updates"
fi
