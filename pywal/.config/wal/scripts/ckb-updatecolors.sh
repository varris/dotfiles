#!/usr/bin/env bash

. $HOME/.cache/wal/colors.sh

ckb_device="/dev/input/ckb1"
wal_color=$(echo "$color4" | sed 's/^#//')

echo "rgb $wal_color" > $ckb_device/cmd
echo "dithering 1" > $ckb_device/cmd
