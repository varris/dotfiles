#!/usr/bin/env bash

usage() {
    echo "usage: $0 smaller/larger"
}

current_gaps_size=$(bspc config window_gap)

if [[ "$#" -ne 1 ]]; then
    usage
else
    if [[ "$1" == "smaller" ]]; then
        bspc config window_gap $(( current_gaps_size - 12 ))
        sed -e "s/^bspc\ config\ window_gap.*/bspc\ config\ window_gap$(printf \ %.0s {0..8}) $(( current_gaps_size - 12 ))/"  -i ~/.config/bspwm/bspwmrc
    elif [[ "$1" == "larger" ]]; then
        bspc config window_gap $(( current_gaps_size + 12 ))
        sed -e "s/^bspc\ config\ window_gap.*/bspc\ config\ window_gap$(printf \ %.0s {0..8}) $(( current_gaps_size + 12 ))/"  -i ~/.config/bspwm/bspwmrc
    else
        usage
    fi
fi
