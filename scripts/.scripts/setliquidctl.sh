#!/bin/bash

. $HOME/.cache/wal/colors.sh

LOGO_COLOR[0]=$(echo "$color3" | sed 's/^#//')
LOGO_COLOR[1]=$(echo "$color8" | sed 's/^#//')
LOGO_COLOR[1]=$(echo "#000000" | sed 's/^#//')

RING_COLOR[0]=$(echo "$color5" | sed 's/^#//')
RING_COLOR[0]=$(echo "$color9" | sed 's/^#//')
RING_COLOR[1]=$(echo "#000000" | sed 's/^#//')

RING_ANIMATION="fading"
LOGO_ANIMATION="fading"

liquidctl set fan speed 30 30  45 50  60 80  70 100 
liquidctl set pump speed 20 50 40 70 50 100

liquidctl set ring color $RING_ANIMATION ${RING_COLOR[@]}
liquidctl set logo color $LOGO_ANIMATION ${LOGO_COLOR[@]}
