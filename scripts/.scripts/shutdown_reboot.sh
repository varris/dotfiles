#!/bin/bash

SELECTION="Reboot\nShutdown"

SELECTED_OPTION=`echo -e $SELECTION | rofi -dmenu`
case $SELECTED_OPTION in
    Reboot*)
        reboot
        ;;
    Shutdown*)
        shutdown
        ;;
esac
