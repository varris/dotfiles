case $TERM in
    st-256color*)
        precmd () {print -Pn "\e]0;st %~\a"}
        ;;
esac

setopt hist_ignore_all_dups
setopt hist_ignore_space
setopt inc_append_history
setopt share_history

alias aur="yay -G"
alias find="fd"
alias gdb="gdb -tui"
alias grep="rg"
alias nf="neofetch"
alias r="vifm"
alias tsr="transmission-remote"
alias um="udiskie-umount -a"
alias watchtsr="watch -n 1 transmission-remote -l"
alias weather="curl https://wttr.in/Vienna"
alias wget="curl -O"

autoload -Uz compinit colors
colors
compinit
zstyle ':completion:*' menu select
zstyle ':completion:*' rehash true

if [[ $UID -ne 0 ]]; then
    export PROMPT="%F{cyan}► %F{reset_color}"
    export RPROMPT="%F{green}%~%F{reset_color}"
else
    export PROMPT="%F{red}(ROOT) %F{reset_color}"
    export RPROMPT="%F{red}%~%F{reset_color}"
fi

export KEYTIMEOUT=1

setopt COMPLETE_ALIASES
setopt auto_cd

autoload zkbd
source ~/.zkbd/$TERM-${${DISPLAY:t}:-$VENDOR-$OSTYPE}

[[ -n ${key[Backspace]} ]] && bindkey "${key[Backspace]}" backward-delete-char
[[ -n ${key[Insert]} ]] && bindkey "${key[Insert]}" overwrite-mode
[[ -n ${key[Home]} ]] && bindkey "${key[Home]}" beginning-of-line
[[ -n ${key[PageUp]} ]] && bindkey "${key[PageUp]}" up-line-or-history
[[ -n ${key[Delete]} ]] && bindkey "${key[Delete]}" delete-char
[[ -n ${key[End]} ]] && bindkey "${key[End]}" end-of-line
[[ -n ${key[PageDown]} ]] && bindkey "${key[PageDown]}" down-line-or-history
[[ -n ${key[Up]} ]] && bindkey "${key[Up]}" up-line-or-search
[[ -n ${key[Left]} ]] && bindkey "${key[Left]}" backward-char
[[ -n ${key[Down]} ]] && bindkey "${key[Down]}" down-line-or-search
[[ -n ${key[Right]} ]] && bindkey "${key[Right]}" forward-char
bindkey "^R" history-incremental-pattern-search-backward

autoload -z edit-command-line
zle -N edit-command-line
bindkey "^X^E" edit-command-line

export DXVK_REPO_DIR="$HOME/repos/dxvk"
export DXVK_BUILD_DIR="$HOME/repos/dxvk-build/dxvk-master"
export WINE_DIR="$HOME/wine"

function wp()
{
    if [[ ! "$#" -ge 1 ]]; then
        echo "Usage: wp wineprefix"
    else
        export WINEPREFIX="$WINE_DIR/$1"
        export DXVK_HUD=version,devinfo,fps
        export PULSE_LATENCY_MSEC=60
        export WINE=$(where wine)
        export WINEDEBUG=fixme-all
        export WINEESYNC=1

        export __GL_SHADER_DISK_CACHE_PATH="$WINEPREFIX"
        export MESA_GLSL_CACHE_DIR="$WINEPREFIX"
        if [[ -d "$WINE_DIR" ]]; then
            if [[ ! -d "$WINEPREFIX/drive_c" ]]; then
                read ""ANSWER"?Prefix $1 does not exist. Create it? "
                if [[ "$ANSWER" = [Yy][Ee][Ss] || "$ANSWER" = [Yy] ]]; then
                    unset ANSWER
                    echo "Creating wine prefix \e[34m$1\e[0m\n"
                    wine wineboot &> /dev/null
                    cd "$WINEPREFIX/drive_c/users/$USER"
                    #so wine doesn't clog up $HOME 
                    for i in "Desktop" "My Documents" "My Music" "My Pictures" "My Videos"; do
                        unlink "$i"
                        mkdir "$i"
                    done
                    cd "$WINEPREFIX/drive_c"
                    echo "done"
                fi
            else
                if [[ $WINE_NO_AUTOSTART -eq 0 || ! -v $WINE_NO_AUTOSTART ]]; then
                    if [[ -f "$WINEPREFIX/wine_autostart.sh" ]]; then
                        . $WINEPREFIX/wine_autostart.sh
                    fi
                fi
                cd "$WINEPREFIX/drive_c"
            fi
        fi
    fi
}

function wpna()
{
    export WINE_NO_AUTOSTART=1
    wp "$@"
    export WINE_NO_AUTOSTART=0
}

function wpdel()
{
    if [[ ! "$#" -eq 1 ]]; then
        echo "Usage: wpdel wineprefix"
    else
        if [[ -d "$WINE_DIR/$1" ]]; then
            read ""ANSWER"?Do you really want to remove prefix $1? "
            if [[ "$ANSWER" = [Yy][Ee][Ss] || "$ANSWER" = [Yy] ]]; then
                rm -R "$WINE_DIR/$1"
                echo "done"
                cd ~
            fi
        else
            echo "Prefix \e[34m$1\e[39m does not exist"
        fi
    fi
}

function wpkill()
{
    if [[ ! "$#" -eq 1 ]]; then
        echo "Usage: wpkill wineprefix"
    else
        export WINEPREFIX="$WINE_DIR/$1"
        wineserver -k
    fi
}

function wpec()
{
    if [[ ! "$#" -eq 1 ]]; then
        echo "Usage: wpec wineprefix"
    else
        if [[ ! -d "$WINE_DIR/$1" ]]; then
            echo "Prefix \e[34m$1\e[39m doesn't exist"
        else
            if [[ -v $EDITOR ]]; then
                $EDITOR "$WINE_DIR/$1/wine_autostart.sh"
            else
                nvim "$WINE_DIR/$1/wine_autostart.sh"
            fi
        fi
    fi
}

function wpmg()
{
    if [[ ! "$#" -eq 1 ]]; then
        echo "usage: wpmg wineprefix"
    else
        cd "$WINE_DIR/$1/drive_c/users/$USER/My Documents/My Games"
    fi
}


function dxvk_rebuild()
{
    if [[ ! -d "$DXVK_REPO_DIR" ]]; then
        echo "DXVK repo doesn't exist. Clone and try again"
    else
        pushd &> /dev/null
        cd "$DXVK_REPO_DIR"
        git fetch origin
        if [[ $(git rev-parse --abbrev-ref HEAD) == "master" ]]; then
            git pull origin master
        fi
        if [[ -d "$DXVK_BUILD_DIR" ]]; then
            rm -R "$DXVK_BUILD_DIR"
        fi
        bash "$DXVK_REPO_DIR/package-release.sh" master "$HOME/repos/dxvk-build" --no-package
        cp "$DXVK_REPO_DIR/utils/setup_dxvk.sh.in" "$DXVK_BUILD_DIR/x32/setup_dxvk.sh"
        sed -i -e "s|@arch@|x86|g" \
               -e "s|@winelib@|false|g" "$DXVK_BUILD_DIR/x32/setup_dxvk.sh"
        cp "$DXVK_REPO_DIR/utils/setup_dxvk.sh.in" "$DXVK_BUILD_DIR/x64/setup_dxvk.sh"
        sed -i -e "s|@arch@|x86_64|g" \
               -e "s|@winelib@|false|g" "$DXVK_BUILD_DIR/x64/setup_dxvk.sh"
        popd &> /dev/null
    fi
}

function dxvk_install()
{
    if [[ ! -d "$DXVK_BUILD_DIR" ]]; then
        echo "DXVK build dir missing. Rebuild and try again"
    else
        if [[ -z "$WINEPREFIX" ]]; then
            echo "WINEPREFIX is not set. Will assume ~/.wine"
            export WINEPREFIX="$HOME/.wine"
        fi
        bash "$DXVK_BUILD_DIR/x32/setup_dxvk.sh" install
        if [[ -d "$WINEPREFIX/drive_c/windows/syswow64" ]]; then
            bash "$DXVK_BUILD_DIR/x64/setup_dxvk.sh" install
        fi
    fi
}

function enable_validation_layers() 
{
    export VK_INSTANCE_LAYERS=VK_LAYER_LUNARG_standard_validation
}

function disable_validation_layers() 
{
    unset VK_INSTANCE_LAYERS
}

function ls()
{
    case "$PWD" in
        */.dotfiles/*) exa -a --color=auto "$@";;
        *) exa --color=auto "$@";;
    esac
}

(cat ~/.cache/wal/sequences &)
source ~/.config/zsh/plugins/*
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
